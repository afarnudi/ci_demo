"""
The title of the programme.

Add a couple of lines for the description as well.
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse

from modules import fibonacci
from modules import create_parser


def main():
    """
    Create desplay message based on user inputs.
    
    This programme uses an argument parser and desplays a message for the user.

    Returns
    -------
    None.

    """
    parser = create_parser()
    args = parser.parse_args()

    result = fibonacci(args.num)
    if args.verbose:
        desplay_message = f"The {args.num}'th Fibonacci number is {result}"
    elif args.quiet:
        desplay_message = str(result)
    else:
        desplay_message = f"Fib({args.num}) = {result}"

    print(desplay_message)


if __name__ == "__main__":
    main()
