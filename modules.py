"""
Your module doc goes here.

Add a couple of lines for the description as well.
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse

def fibonacci(n):
    """
    Calculate numbers in the Fibonacci sequence.

    Parameters
    ----------
    n : int
        The position of the number in the sequence.

    Returns
    -------
    a : int
        The n'th number in the Fibonacci sequence.

    """
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return a

def create_parser():
    """
    Parse user inputs.

    Returns
    -------
    parser : argparse.parser
        DESCRIPTION.

    """
    parser = argparse.ArgumentParser(
        "Fibonacci Printer", description="Prints the Fibonacci numbers in the termianl."
    )
    parser.add_argument("num", help="Print the n'th Fibonacci number.", type=int)

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-v", "--verbose", action="store_true")
    group.add_argument("-q", "--quiet", action="store_true")
    return parser